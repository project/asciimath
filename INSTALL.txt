
INTRODUCTION
The asciimath module provides a wrapper around Steven Chan's ASCIIMathPHP code in order to provide a way 
to create MathML content in Drupal.  You will need ASCIIMathPHP and possibly an XSL style sheet.  These are 
not included. You can these items at the following locations:

ASCIIMathPHP:
* Steven Chan's site, JC Physics, has the official release: http://www.jcphysics.com/ASCIIMath/index.php
* My site, Cowlix, has a version which was updated in the course of writing this module. It
  can be found at http://www.cowlix.com/site/node/245.
Either of these versions should work fine with this module.

Style sheets:
You may need an XSL style sheet in order to properly display MathML content.  You can get these from the 
W3C site at http://www.w3.org/Math/XSL/.
 

STEPS
There are several steps you will need to complete while installing this.  

1. Extract the asciimath directory from the archive into the modules directory so that you have:
	.../modules/asciimath/asciimath.module
2. Obtain ASCIIMathPHP.class.php and ASCIIMathPHP.cfg.php from one of the sites listed above. Place
   these files in the modules/asciimath directory.
3. Obtain the XSL style sheets you want to use and place them somewhere accessible, either in the 
   modules/asciimath directory or your theme, or whetherever you keep the rest of your stylesheets.
4. Go into Drupal's module administration and enable the module.
5. Modify your page.tpl.php (or whatever you are using to define your page layouts) so that MathML
   page are served up as XML.  There are some examples at http://www.cowlix.com/site/node/245. 
6. Go admin/filters and add a new input input format titled something like "ASCIIMath".
7. Choose the Configure option for your new input format and add the asciimath filter to it. You may
   optionally add other filters to the ASCIIMath input format but be careful with the order.  In particular,
   the line break converter filter must be before ASCIIMath and freelinking must be after ASCIIMath.

That's all there is to installing the module, but there are a few things you might want to do:
* Make very sure that your pages validate.  Mozilla is very unforgiving with XML content that doesn't validate.
* You may need to get extra fonts for Mozilla: http://www.mozilla.org/projects/mathml/fonts/
* You may need to get a plugin for IE: http://www.dessci.com/en/products/mathplayer/download.htm